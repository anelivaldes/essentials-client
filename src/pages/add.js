import React, {useEffect} from "react";
import {makeStyles} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import GraphQL from "../services/graphQL";
import {useSpreadState} from "../components/hooks";
import Loading from "../commons/Loading";
import OperationResult from "../commons/OperationResult";
import FormBook from "../components/FormBook";


const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        margin: '3em 0'
    },
    title: {
        marginBottom: '1em'
    }
}));

export default function Add() {

    const classes = useStyles();

    const [data, setData] = useSpreadState({
        error: false,
        loading: false,
        book: null,
        showOpResult: false
    });

    const {error, loading, book, showOpResult} = data;

    useEffect(() => {
        if (loading) {
            const subscription = GraphQL.addBook(book).subscribe(result => {
                if (result.data.addBook) {
                    const book = result.data.addBook;
                    setData({book, loading: false, showOpResult: true});
                } else {
                    const error = result.errors[0];
                    setData({loading: false, error, showOpResult: true});
                }
            }, error => {
                console.log(error);
                setData({loading: false, error, showOpResult: true});
            });


            return () => {
                subscription.unsubscribe();
            };
        }
    }, [loading, book, setData]);

    const onSubmit = (book) => {
        setData({book, loading: true})
    };


    return (
        <>
            <div className={classes.root}>
                <Typography variant="h5" className={classes.title} gutterBottom component='h5'>
                    Add a new Book
                </Typography>
                <FormBook btnLabel={'Salvar'} onSubmit={onSubmit}/>
                <Loading loading={loading}/>
                <OperationResult show={showOpResult}
                                 error={error}
                                 success={book}
                                 dismissHandler={() => {
                                     setData({showOpResult: false})
                                 }}
                />
            </div>
        </>
    )
}
