import React, {useEffect, useRef} from "react";
import {Link, useParams} from "react-router-dom";
import GraphQL from "../services/graphQL";
import {useSpreadState} from "../components/hooks";
import Button from '@material-ui/core/Button';


export default function Oldedit() {
    let {book_id} = useParams();
    let inputTitleRef = useRef(null);
    let inputAuthorRef = useRef(null);
    const [data, setData] = useSpreadState({
        error: false,
        loading: false,
        book: null,
        message: null,
        updating: false,
        showNotification: null
    });


    const {error, loading, book, message, updating, showNotification} = data;
    useEffect(() => {
        setData({loading: true});
        const subscription = GraphQL.getBook(book_id).subscribe(result => {
            if (result.data.findBookById) {
                setData({
                        error: false,
                        loading: false,
                        book: result.data.findBookById
                    }
                );
                inputTitleRef.current.value = result.data.findBookById.title;
                inputAuthorRef.current.value = result.data.findBookById.author.name
            } else {
                if (result.errors.length > 0) {
                    setData({
                        error: false,
                        loading: false,
                        message: result.errors[0].message,
                        book: null
                    })
                }
            }
        }, error => {
            setData({error: error, loading: false, book: null})
        });
        return () => subscription.unsubscribe();
    }, []);

    useEffect(() => {
        if (updating) {
            const title = inputTitleRef.current.value;
            const author = inputAuthorRef.current.value;
            const subscription = GraphQL.updateBook({id: book_id, title, author_name: author}).subscribe(result => {
                if (result.data.updateBook) {
                    setData({
                            error: false,
                            loading: false,
                            book: result.data.updateBook,
                            updating: false,
                            showNotification: {message: `Book: ${result.data.updateBook.title} was successfully updated`}
                        }
                    );
                } else {
                    if (result.errors.length > 0) {
                        setData({
                            error: false,
                            loading: false,
                            message: result.errors[0].message,
                            book: null,
                            updating: false
                        });
                        console.error(message);
                    }
                }
            }, error => {
                setData({error: error, loading: false, book: null, updating: false})
            });
            return () => subscription.unsubscribe();
        }
    }, [updating]);

    const addNotification = (content) => {
        setTimeout(() => {
            setData({showNotification: null})
        }, 1000);
        return ''
    };

    let notifications = showNotification ? addNotification(showNotification.message) :'';
    const updateBook = () => {
        setData({updating: true})
    };


    let content;
    if (error) {
        content = <h2>An error happened</h2>;
    } else if (loading) {
        content = <h2>Loading...</h2>;

    } else if (book) {
        content = <form>
            <label>Title:
                <input name="title" ref={inputTitleRef}/>
            </label>
            <label>Author:
                <input name="author" ref={inputAuthorRef}/>
            </label>
            <Button variant="contained" color="primary" disabled={updating} onClick={updateBook}>
                Update
            </Button>
        </form>
    } else {
        content = message ? <h2>{message}</h2> : <h2>Error</h2>
    }
    return (
        <>
            <h1>Editing</h1>
            {content}
            {notifications}
            <Link to="/" style={{marginTop: '30px', display: 'block'}}>Back to list of books</Link>
        </>
    );
}
