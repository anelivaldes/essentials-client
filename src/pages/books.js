import React, {useEffect} from "react";
import Add from "./add";
import GraphQL from "../services/graphQL";
import {useSpreadState} from "../components/hooks";

export default function Books() {
    const [data, setData] = useSpreadState({
        error: false,
        loading: false,
        books: [],
        count: 0
    });

    const {loading, deletingId, addingBook} = data;

    useEffect(() => {
        setData({loading: true});
        const subscription = GraphQL.getAllBooks().subscribe(result => {
            setData({
                error: false,
                loading: false,
                books: result.data.getAllBooks,
                count: result.data.getAllBooks.length
            });
        }, error => {
            setData({error: error, loading: false, books: []})
        });
        return () => subscription.unsubscribe();
    }, []);

    useEffect(() => {
        if (deletingId) {
            const subscription = GraphQL.deleteBook(deletingId).subscribe(result => {
                let done = false;
                if (result.data.deleteBookById) {
                    setData(({books}) => {
                        const index = books.findIndex(({id}) => id === deletingId);
                        if (index !== -1) {
                            done = true;
                            const newBooks = [...books];
                            newBooks.splice(index, 1);
                            return {books: newBooks, deletingId: null};
                        }
                    });
                }
                if (!done) {
                    setData({deletingId: null});
                }
            }, error => console.log(error));

            return () => subscription.unsubscribe();
        }
    }, [deletingId]);

    useEffect(() => {
        if (addingBook) {
            const subscription = GraphQL.addBook(addingBook).subscribe(result => {
                let done = false;
                if (result.data.addBook) {
                    setData(({books}) => {
                        return {books: [result.data.addBook, ...books,], addingBook: null};
                    });
                }
                if (!done) {
                    setData({addingBook: null});
                }
            }, error => {
                console.log(error);
                setData({error: error});
            });

            return () => subscription.unsubscribe();
        }
    }, [addingBook]);

    const deleteBook = deletingId => setData({deletingId});

    let drop;
    if (loading || deletingId || addingBook) {
        drop = <div style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            background: 'beige',
            opacity: 0.5
        }}/>;
    }

    return (
        <div style={{position: 'relative'}}>
            <Add />
        </div>
    );
}
