import React, {useEffect} from "react";
import {makeStyles} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import GraphQL from "../services/graphQL";
import {useSpreadState} from "../components/hooks";
import Loading from "../commons/Loading";
import OperationResult from "../commons/OperationResult";
import FormBook from "../components/FormBook";
import {Link, useParams} from "react-router-dom";

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    margin: '3em 0'
  },
  title: {
    marginBottom: '1em'
  }
}));

export default function Edit() {

  const classes = useStyles();
  let {book_id} = useParams();

  const [data, setData] = useSpreadState({
    error: false,
    loading: false,
    book: null,
    updating: false,
    showOpResult: false
  });

  const {error, loading, book, showOpResult, updating} = data;

  useEffect(() => {
    setData({loading: true});
    const subscription = GraphQL.getBook(book_id).subscribe(result => {
      if (result.data.findBookById) {
        setData({
            error: false,
            loading: false,
            book: result.data.findBookById
          }
        );
      } else {
        if (result.errors.length > 0) {
          setData({
            error: true,
            loading: false,
            message: result.errors[0].message,
            book: null,
            showOpResult: true
          })
        }
      }
    }, error => {
      setData({error: error, loading: false, book: null, showOpResult: true})
    });
    return () => subscription.unsubscribe();
  }, []);

  useEffect(() => {
    if (updating) {
      const title = book.title;
      const author = "";
      const subscription = GraphQL.updateBook({id: book_id, title, author_name: author}).subscribe(result => {
        if (result.data.updateBook) {
          const book = result.data.updateBook;
          setData({book, loading: false, showOpResult: true, updating: false});
        } else {
          const error = result.errors[0];
          setData({loading: false, error, showOpResult: true, updating: false});
        }
      }, error => {
        console.log(error);
        setData({loading: false, error, showOpResult: true});
      });
      return () => {
        subscription.unsubscribe();
      };
    }
  }, [updating]);

  const onSubmit = (book) => {
    setData({book, updating: true})
  };


  return (
    <>
      <div className={classes.root}>
        <Typography variant="h5" className={classes.title} gutterBottom component='h5'>
          Edit a book
        </Typography>
        <FormBook book={book} btnLabel={'Update'} onSubmit={onSubmit}/>
        <Loading loading={loading}/>
        <OperationResult show={showOpResult}
                         error={error}
                         success={book}
                         dismissHandler={() => {
                           setData({showOpResult: true})
                         }}
        />
      </div>
    </>
  )
}
