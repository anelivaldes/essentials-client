import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import React, {useEffect, useRef} from "react";
import SaveIcon from '@material-ui/icons/Save';
import {useSpreadState} from "./hooks";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

export default function FormBook(props) {

    const inputTitleRef = useRef(null);
    const inputAuthorRef = useRef(null);
    const inputNationalityRef = useRef(null);
    const inputEditorialRef = useRef(null);
    const inputYearRef = useRef(null);

    const [form, setForm] = useSpreadState({
        errors: {},
        helpers: {}
    });

    const [data, setData] = useSpreadState(props);
    const {errors, helpers} = form;

    const {btnLabel, onSubmit, book} = data;

    useEffect(() => {
        setData(props);
    }, [props]);

    useEffect(() => {
        if (book) {
            const titleInput = inputTitleRef.current;
            const author_nameInput = inputAuthorRef.current;
            const editorialInput = inputEditorialRef.current;
            const nationalityInput = inputNationalityRef.current;

            titleInput.value = book.title
        }
    });



    function validate({title, author_name, editorial, nationality}) {
        const errors = {};
        const helpers = {};
        if (title.length === 0) {
            errors.title = true;
            helpers.title = 'Required field'
        }
        // TODO: Add more validators
        return [errors, helpers];
    }

    const submit = () => {
        const title = inputTitleRef.current.value;
        const author_name = inputAuthorRef.current.value;
        const editorial = inputEditorialRef.current.value;
        const nationality = inputNationalityRef.current.value;

        const book = {
            title,
            author_name,
            editorial,
            nationality
        };

        const [errors, helpers] = validate(book);
        if (Object.keys(errors).length > 0) {
            setForm({errors, helpers})
        } else {
            onSubmit(book)
        }
    };

    return (
        <>
            <form noValidate autoComplete="off">
                <Grid container spacing={3} component='div'>
                    <Grid item xs={12} sm={4} md={6} component='div'>
                        <TextField error={errors.title} label="Title" required fullWidth
                                   variant="outlined"
                                   helperText={helpers.title}
                                   inputRef={inputTitleRef}
                        />
                    </Grid>
                    <Grid item xs={12} sm={4} md={6} component='div'>
                        <TextField label="Editorial" fullWidth variant="outlined" inputRef={inputEditorialRef}/>
                    </Grid>
                    <Grid item xs={12} sm={4} md={6} component='div'>
                        <TextField label="Year" fullWidth variant="outlined" inputRef={inputYearRef}/>
                    </Grid>
                    <Grid item xs={12} component='div'>
                        <Accordion>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon/>}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                                href=''>
                                <Typography component='div'>Author</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Grid container spacing={3} component='div'>
                                    <Grid item xs={12} sm={4} md={6} component='div'>
                                        <TextField label="Name" required fullWidth variant="outlined"
                                                   inputRef={inputAuthorRef}/>
                                    </Grid>
                                    <Grid item xs={12} sm={4} md={6} component='div'>
                                        <TextField label="Nationality" fullWidth variant="outlined"
                                                   inputRef={inputNationalityRef}/>

                                    </Grid>
                                </Grid>
                            </AccordionDetails>
                        </Accordion>
                    </Grid>
                    <Grid item xs={12} component='div'>
                        <Button
                            variant="contained"
                            color="primary"
                            size="large"
                            startIcon={<SaveIcon/>}
                            onClick={submit}
                            href=''>
                            {btnLabel}
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </>
    )
}
