import CircularProgress from "@material-ui/core/CircularProgress";
import OperationResult from "../commons/OperationResult";
import React, {useEffect} from "react";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import {Book} from "./Book";
import {useSpreadState} from "./hooks";


const useStyles = makeStyles(() => ({
    container: {
        flexGrow: 1,
        margin: '3em 0',
    },
    title: {
        marginBottom: '1em'
    }
}));

function BookList(props) {
    const classes = useStyles();
    let content;

    const [data, setData] = useSpreadState(props);
    const {error, loading, books, view} = data;

    const afterDelete = (id) => {
        const other = books.filter((b) => b.id !== id);
        setData({books: other})
    };

    useEffect(() => {
        setData(props)
    }, [props]);

    if (error) {
        content = <OperationResult show={true} error={true}/>
    } else if (loading) {
        content = <CircularProgress/>
    } else {
        if (books && books.length > 0) {
            content = books.map((book) => <Book book={book} key={book.id} view={view} afterDelete={afterDelete}/>
            )
        } else {
            if (books && books.length === 0) {
                content = <Alert severity="info">No results</Alert>;
                content = <Grid item xs={12} sm={6} md={12} component='div'>{content}</Grid>
            } else {
                content = ''
            }
        }
    }
    return (<>
        <div className={classes.container}>
            <Grid container spacing={3} component='div'>
                {content}
            </Grid>
        </div>
    </>)
}

export default BookList;
