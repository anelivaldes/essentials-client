import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button";
import React, {useEffect} from "react";
import {useSpreadState} from "./hooks";
import GraphQL from "../services/graphQL";
import Loading from "../commons/Loading";
import OperationResult from "../commons/OperationResult";

export default function DeleteConfirmation({book, open, onClose, afterDelete}) {
    const [data, setData] = useSpreadState({
        error: false,
        deleting: false,
        deletedCount: 0,
        showOpResult: false
    });
    const {error, deleting, showOpResult} = data;

    useEffect(() => {
        if (deleting) {
            const subscription = GraphQL.deleteBook(book.id).subscribe(result => {
                if (result.data.deleteBookById) {
                    const book = result.data.deleteBookById;
                    setData({deletedCount: 1, deleting: false, showOpResult: true});
                } else {
                    const error = result.errors[0];
                    setData({deleting: false, error, showOpResult: true});

                }
            }, error => {
                console.log(error);
                setData({deleting: false, error, showOpResult: true});
            });
            return () => subscription.unsubscribe();
        }
    }, [deleting]);

    return (<>
        <Dialog
            open={open}
            onClose={onClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{`Are you sure you want to delete ${book.title}?`}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {'This action cannot be undone'}
                </DialogContentText>
                <Loading loading={deleting}/>
                <OperationResult show={showOpResult}
                                 error={error}
                                 success={book}
                                 dismissHandler={() => {
                                     setData({showOpResult: false});
                                     afterDelete(book.id);
                                     onClose()
                                 }}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    Cancel
                </Button>
                <Button onClick={() => {
                    setData({deleting: true})
                }} color="primary" autoFocus>
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>
    </>)
}
