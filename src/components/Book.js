import {useHistory} from "react-router-dom";
import React from "react";
import Grid from "@material-ui/core/Grid";
import BookListView from "./BookListView";
import BookCardView from "./BookCardView";

export function Book({book, view, afterDelete}) {
    const history = useHistory();
    const edit = (id) => {
        history.push(`edit/${id}`);
    };
    let bookView = '';
    if (view === 'list') {
        bookView = <BookListView book={book} editFn={edit}/>
    } else {
        bookView = <BookCardView book={book} editFn={edit} afterDelete={afterDelete}/>
    }
    return <Grid item xs={12} sm={6} md={view === 'list' ? 12 : 4} component='div'>
        {bookView}
    </Grid>
}
