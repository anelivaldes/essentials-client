import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions/CardActions";
import Button from "@material-ui/core/Button";
import React from "react";
import DeleteConfirmation from "./DeleteConfirmation";

export default function BookCardView({book, editFn, afterDelete}) {
    const [openDeleteDialog, setOpenDeleteDialog] = React.useState(false);

    const handleClickOpen = () => {
        setOpenDeleteDialog(true);
    };

    const handleClose = () => {
        setOpenDeleteDialog(false);
    };
    return (
        <>
            <Card>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {book.title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {book.author.name}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" color="primary" onClick={() => {
                        editFn(book.id)
                    }}>
                        Edit
                    </Button>
                    <Button size="small" color="primary" onClick={handleClickOpen}>
                        Delete
                    </Button>
                </CardActions>
            </Card>
            <DeleteConfirmation book={book} open={openDeleteDialog} onClose={handleClose} afterDelete={afterDelete}/>
        </>
    )
}
