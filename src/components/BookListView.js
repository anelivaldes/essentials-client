import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon/ListItemIcon";
import FolderIcon from '@material-ui/icons/Folder'
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import React from "react";

export default function BookListView({book, editFn}) {
    return (
        <>
            <ListItem button>
                <ListItemIcon>
                    <FolderIcon/>
                </ListItemIcon>
                <ListItemText primary={book.title} onClick={() => {
                    editFn(book.id)
                }}/>
            </ListItem>
        </>
    )
}
