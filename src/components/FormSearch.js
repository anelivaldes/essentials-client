import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField/TextField";
import React, {useEffect, useRef, useState} from "react";
import {makeStyles} from "@material-ui/core";
import {useSpreadState} from "./hooks";
import {fromEvent, of} from "rxjs";
import {debounceTime, map, switchMap} from "rxjs/operators";
import GraphQL from "../services/graphQL";
import BookList from "./BookList";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ViewListIcon from '@material-ui/icons/ViewList';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        margin: '3em 0'
    },
    title: {
        marginBottom: '1em'
    },
    form: {
        marginBottom: '2em'
    }
}));

export default function FormSearch() {
    const classes = useStyles();
    const inputSearchRef = useRef(null);
    const [data, setData] = useSpreadState({
        error: false,
        loading: false,
        books: null
    });
    const [view, setView] = useState('list');

    const handleView = (event, newAlignment) => {
        if (newAlignment) {
            setView(newAlignment);
            // TODO: Remove duplication for success and error handlers and 3 characters required to search
            setData({loading: true});
            GraphQL.findBookByTitle(inputSearchRef.current.value, newAlignment).subscribe((results) => {
                    if (results) {
                        if (results.data.findBookByTitle) {
                            setData({
                                loading: false,
                                books: results.data.findBookByTitle,
                            })
                        } else {
                            if (results.errors.length > 0) {
                                setData({
                                    error: false,
                                    loading: false,
                                    message: results.errors[0].message,
                                    books: []
                                })
                            }
                        }
                    } else {
                        setData({
                            error: false,
                            loading: false,
                            books: null
                        })
                    }
                },
                error => {
                    setData({error: error, loading: false})
                })
        }
    };
    const {loading} = data;

    useEffect(() => {
        // streams
        const keyup$ = fromEvent(inputSearchRef.current, 'keyup');

        // wait .5s between keyups to emit current value
        keyup$
            .pipe(
                map((i) => i.currentTarget.value),
                debounceTime(500),
                switchMap((r) => {
                    if (r.length >= 3) {
                        setData({loading: true});
                        return GraphQL.findBookByTitle(r, view)
                    } else {
                        return of(false);
                    }
                })
            )
            .subscribe((results) => {
                    if (results) {
                        if (results.data.findBookByTitle) {
                            setData({
                                loading: false,
                                books: results.data.findBookByTitle,
                            })
                        } else {
                            if (results.errors.length > 0) {
                                setData({
                                    error: false,
                                    loading: false,
                                    message: results.errors[0].message,
                                    books: []
                                })
                            }
                        }
                    } else {
                        setData({
                            error: false,
                            loading: false,
                            books: null
                        })
                    }
                },
                error => {
                    setData({error: error, loading: false})
                });
    }, [loading]);

    return (<>
            <div className={classes.root}>
                <Typography variant="h5" className={classes.title} gutterBottom component='h5'>
                    Search
                </Typography>
                <form className={classes.form} noValidate autoComplete="off">
                    <Grid container spacing={3} component='div'>
                        <Grid item xs={6} component='div'>
                            <TextField label="Type to search" required fullWidth
                                       variant="outlined"
                                       inputRef={inputSearchRef}/>
                        </Grid>
                        <Grid item xs={6} component='div'>
                            <ToggleButtonGroup
                                value={view}
                                exclusive
                                onChange={handleView}
                                aria-label="text alignment"
                            >
                                <ToggleButton value="list" aria-label="left aligned">
                                    <ViewListIcon/>
                                </ToggleButton>
                                <ToggleButton value="card" aria-label="centered">
                                    <ViewModuleIcon/>
                                </ToggleButton>
                            </ToggleButtonGroup>
                        </Grid>
                    </Grid>
                </form>
                <Divider/>
                <BookList {...data} view={view}/>
            </div>
        </>
    )
}
