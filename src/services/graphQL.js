import {from} from "rxjs";

const config = {
    url: process.env.REACT_APP_SERVER_ULR, options: {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'Node',
        },
    }
};

const QUERIES = {
    getAllBooks: {
        query: `{
                  getAllBooks {
                    title
                    id
                    author {
                      name
                    }
                  }
                }`
    },
    deleteBook: {
        query: `mutation deleteById($id: String!) {
                    deleteBookById(id: $id){title id}
                }`
    },
    addBook: {
        query: `mutation addBook($title: String!, $editorial: String, $year: Int, $author_name: String!, $nationality:                  String!) {
                    addBook(book: {title: $title, editorial: $editorial, year: $year}, author: {name: $author_name,                         nationality: $nationality}) 
                        {
                            title
                            id
                            author {
                              name
                            }
                       }
               }`
    },
    getBook:{
        query: `query findBookById($id: ID!){ 
                  findBookById(id: $id) {
                    id
                    title
                    author{
                        name
                    }
                  }
                }`
    },
    updateBook:{
        query: `mutation updateBook($id: ID!, $title: String!, $editorial: String, $year: Int, $author_name: String!,                   $nationality: String!) {
                  updateBook(id: $id,book: {title: $title, editorial: $editorial, year: $year}, author: {name:                          $author_name, nationality: $nationality}) {
                        id
                        title
                        author{
                            name
                        }
                    }
                }`
    },
    findBookByTitle:{
        query: {
            list: `query findBookByTitle($title: String!){
              findBookByTitle(title: $title) {
                title
                id
              }
            }`,
            card: `query findBookByTitle($title: String!){
              findBookByTitle(title: $title) {
                title
                year
                id
                author {
                  name
                  nationality
                }
              }
            }`
        }
    }
};

export default {
    getAllBooks: () => {
        const body = JSON.stringify({
            query: QUERIES.getAllBooks.query
        });
        return from(
            fetch(config.url, {body, ...config.options}).then(response => response.json())
        );
    },
    getBook: (id) => {
        const body = JSON.stringify({
            query: QUERIES.getBook.query,
            variables: `{
                    "id": "${id}"
                  }`
        });
        return from(fetch(config.url, {body, ...config.options}).then(response => Promise.resolve(response.json()))
        );
    },
    deleteBook: (id) => {
        const body = JSON.stringify({
            query: QUERIES.deleteBook.query,
            variables: `{
                    "id": "${id}"
                  }`
        });
        return from(fetch(config.url, {body, ...config.options}).then(response => Promise.resolve(response.json()))
        );
    },
    addBook: ({title = "No title", editorial = "", year = 0, author_name = "", nationality = ""}) => {
        const body = JSON.stringify({
            query: QUERIES.addBook.query,
            variables: `{
                          "title": "${title}",
                          "editorial": "${editorial}",
                          "year": ${year},
                          "author_name": "${author_name}",
                          "nationality": "${nationality}"
                        }`
        });
        return from(fetch(config.url, {body, ...config.options}).then(response => Promise.resolve(response.json()))
        );
    },
    updateBook: ({id,title = "No title", editorial = "", year = 0, author_name = "", nationality = ""}) => {
        const body = JSON.stringify({
            query: QUERIES.updateBook.query,
            variables: `{
                          "id": "${id}",
                          "title": "${title}",
                          "editorial": "${editorial}",
                          "year": ${year},
                          "author_name": "${author_name}",
                          "nationality": "${nationality}"
                        }`
        });
        return from(fetch(config.url, {body, ...config.options}).then(response => Promise.resolve(response.json()))
        );
    },
    findBookByTitle: (title, view) => {
        const body = JSON.stringify({
            query: QUERIES.findBookByTitle.query[view],
            variables: `{
                    "title": "${title}"
                  }`
        });
        return from(fetch(config.url, {body, ...config.options}).then(response => Promise.resolve(response.json()))
        );
    },
}
