import React from "react";
import Alert from "@material-ui/lab/Alert/Alert";

export default function OperationResult({
                                            show,
                                            error,
                                            success,
                                            dismissHandler = () => {
                                            },
                                            timeOut = 1000
                                        }) {
    if (show) {
        let content = '';
        if (error) {
            content = <Alert severity='error'>An error happened</Alert>
        } else {
            if (success) {
                content = <Alert severity='success'>Operation success</Alert>
            }
        }
        setTimeout(() => {
            dismissHandler()
        }, timeOut);
        return <>
            <div style={{'margin': '2rem 0'}}>{content}</div>
        </>
    } else {
        return null
    }
}
