import React, {Suspense} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Header from "./layout/Header";
import Main from "./layout/Main";
import CssBaseline from "@material-ui/core/CssBaseline";
import Add from "./pages/add";
import FormSearch from "./components/FormSearch";
import Edit from "./pages/edit";

function App() {
    return (
        <>
            <CssBaseline />
            <Router>
                <Header/>
                <Main>
                    <Suspense fallback={<div>Loading...</div>}>
                        <Switch>
                            <Route exact path="/" component={FormSearch}/>
                            <Route path="/edit/:book_id" component={Edit}/>
                            <Route path="/add" component={Add}/>
                        </Switch>
                    </Suspense>
                </Main>
            </Router>
        </>
    )

}

export default App;
