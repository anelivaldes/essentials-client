import "./Main.scss";
import Container from "@material-ui/core/Container";
import React from "react";
export default function Main({children}) {
    return (<>
        <Container maxWidth="md" component='div'>
            {children}
        </Container>
    </>)
}
